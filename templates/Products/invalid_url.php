<?= $this->fetch('header'); ?>
<br>
<?= $this->Flash->render() ?>
<br>
<div class="container">
	<div class="jumbotron">
		<?php if (!is_null($message)): ?>
			<center><h3 style="font-family: times new roman"><?= h($message) ?></h3></center>
			<center><h3 style="font-family: times new roman"><a href="<?= $this->Url->build(['_name' => 'HomePage']) ?>">Back To Home Page</a></h3></center>
		<?php endif; ?>
	</div>
</div>