<?= $this->fetch('header'); ?>
<br>
<?= $this->Flash->render() ?>
<br>
<div class="container">
    <div class="jumbotron">
        <div id="loadData">
            <button type="button" class="btn btn-success" ><a href="<?= $this->Url->build(['_name' => 'AddProduct']) ?>" style="color: white;text-decoration: none;">Add Product</a></button>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered" style="border:1px solid black">
                    <thead>
                        <tr align="center">
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('name') ?></th>
                            <th><?= $this->Paginator->sort('description') ?></th>
                            <th><?= $this->Paginator->sort('price') ?></th>
                            <th><?= $this->Paginator->sort('quantity') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $product): ?>
                            <tr align="center">

                                <td><?= $this->Number->format($product->id) ?></td>
                                <td><?= h($product->name) ?></td>
                                <td><?= h($product->description) ?></td>
                                <td><?= h($product->price) ?></td>
                                <td><?= h($product->quantity) ?></td>
                                <td class="actions">
                                    <?= $this->Html->image("edit.png", [
                                        "alt" => "Edit",
                                        'width' => "20",
                                        'url' => [
                                            '_name' => 'EditProduct',
                                            $product->slug,
                                        ]
                                    ]); ?>
                                    <?= $this->Html->image("delete.png", [
                                        "alt" => "Delete",
                                        "id" => "userDelete",
                                        'width' => "20",
                                        'data-modal-url' => $this->Url->build(["_name" => "DeleteProduct",$product->slug])
                                    ]); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="paginator">
                <ul class="pagination justify-content-end">
                    <?= $this->Paginator->prev("<<") ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(">>") ?>
                </ul>
                <p align="right"><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click',"#userDelete",function() {
        var url = $(this).data("modal-url");
        bootbox.confirm('Are you sure?', function(result) { 
            if (result) {
                $(document).find("#wait").css("display", "block");
                $.ajax({
                    url : url,
                    success: function(output) {
                        $('.alert-success').show();
                        $('.alert-success').text(output);
                        $('.container').empty();
                        $(".container").load(" .container",function(){
                            $(document).find("#wait").css("display", "none");
                        }); 
                    },
                    error: function(){
                        $('.alert-success').show();
                        $('.alert-success').text("Internal Server Error. Please try again !");
                        $(document).find("#wait").css("display", "none");
                    }
                });
            }
        });
    });
</script>


