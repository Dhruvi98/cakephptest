<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <?= $this->fetch('header'); ?>
    <br>
    <?= $this->Flash->render() ?>
    <br>
    <div class="container">
        <div class="jumbotron">
            <center><h3 style="font-family: times new roman;">Add Product</h3></center>
            <?php
            echo $this->Form->create($product,['type' => 'file']);
            echo $this->Form->controls(
                [
                    'name' => [
                        'name' => 'name',
                        'placeholder' => "Mobile", 
                        'required' => false,
                        'label' => 'Product Name',
                        'class' => ($this->Form->isFieldError('name')) ? 'form-control is-invalid' : 'form-control form-control-sm'
                    ],
                    'description' => [
                        'type' => 'textarea',
                        'placeholder' => "description...", 
                        'required' => false,
                        'class' => ($this->Form->isFieldError('description')) ? 'form-control is-invalid' : 'form-control form-control-sm'
                    ],
                    'price' => [
                        'type' => 'text',
                        'placeholder' => "000.00", 
                        'required' => false,
                        'class' => ($this->Form->isFieldError('price')) ? 'form-control is-invalid' : 'form-control form-control-sm'
                    ],
                    'quantity' => [
                        'type' => 'text',
                        'placeholder' => "50", 
                        'required' => false,
                        'class' => ($this->Form->isFieldError('quantity')) ? 'form-control is-invalid' : 'form-control form-control-sm'
                    ],
                    'image' => [
                        'type' => 'file',
                        'required' => false,
                        'class' => ($this->Form->isFieldError('image')) ? 'form-control is-invalid' : 'form-control form-control-sm form-control-file'
                    ],
                ],
                ['legend' => '']
            );
            echo $this->Html->image('preview.png',['width' => '200' ,'id' => 'photo']);
            echo "<br>";
            echo "<br>";
            ?>
            <div align="center">
                <?= $this->Form->submit('Add Product', ['class' => 'btn btn-primary' , 'name' => 'submit']);?>
            </div>
            <?php     
            echo $this->Form->end();
            ?>
        </div>
    </div>
</body>
</html>