<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <?= $this->fetch('header'); ?>
    <br>
    <?= $this->Flash->render() ?>
    <br>
    <div class="container">
        <div class="jumbotron">
            <center><h3 style="font-family: times new roman;">Edit Product</h3></center>
            <?php
            echo $this->Form->create($product,['type' => 'file']);
            echo $this->Form->controls(
                [
                    'name' => [
                        'name' => 'name',
                        'placeholder' => "Mobile", 
                        'required' => false,
                        'label' => 'Product Name',
                        'class' => ($this->Form->isFieldError('name')) ? 'form-control is-invalid' : 'form-control form-control-sm'
                    ],
                    'description' => [
                        'type' => 'textarea',
                        'placeholder' => "description...", 
                        'required' => false,
                        'class' => ($this->Form->isFieldError('description')) ? 'form-control is-invalid' : 'form-control form-control-sm'
                    ],
                    'price' => [
                        'type' => 'text',
                        'placeholder' => "000.00", 
                        'required' => false,
                        'class' => ($this->Form->isFieldError('price')) ? 'form-control is-invalid' : 'form-control form-control-sm'
                    ],
                    'quantity' => [
                        'type' => 'text',
                        'placeholder' => "50", 
                        'required' => false,
                        'class' => ($this->Form->isFieldError('quantity')) ? 'form-control is-invalid' : 'form-control form-control-sm'
                    ],
                    'image' => [
                        'type' => 'file',
                        'required' => false,
                        'class' => ($this->Form->isFieldError('image')) ? 'form-control is-invalid' : 'form-control form-control-sm form-control-file'
                    ],
                ],
                ['legend' => '']
            );
            echo $this->Html->image('uploads/'.$product->image,['width' => '200' ,'id' => 'photo']);
            echo "<br>";
            echo "<br>";
            ?>
            <div align="center">
                <?= $this->Form->submit('Add Product', ['class' => 'btn btn-primary' , 'name' => 'submit']);?>
            </div>
            <?php     
            echo $this->Form->end();
            ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    $(document).on("change","#image",function() {

        var changedFile = $("#image").val();

        var data = changedFile.replace(changedFile.substring(0, 12),"");
        $("#imageError").text("");
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $("#photo").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
    });
</script>
</html>