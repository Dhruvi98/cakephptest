<?= $this->fetch('header'); ?>
<br>
<?= $this->Flash->render() ?>
<br>
<div class="container">
	<div class="jumbotron">
		<center><h3 style="font-family: times new roman">Welcome ! <?= $this->Identity->get('name') ?></h3></center>
		<br>
		<center><button class="btn btn-primary"><a href="<?= $this->Url->build(['_name' => 'ViewAllProduct']) ?>" style="color: white;text-decoration: none;">Browse Products</a></button></center>
	</div>
</div>
