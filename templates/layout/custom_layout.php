<!DOCTYPE html>
<html>
<head>
	<title>Product Management System</title>
	<?php $url = env('REQUEST_URI'); ?>

	<?= $this->Html->script("jquery.min.js"); ?>
	<?= $this->Html->script("bootstrap.min.js"); ?>
	<?= $this->Html->css("bootstrap.min.css"); ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<?php if (
		$url == $this->Url->build(['_name' => 'ViewAllProduct']) || 
		$url == $this->Url->build(['_name' => 'ViewAllCartProduct']) || 
		$url == $this->Url->build(['_name' => 'ViewAllFavouriteProduct'])
	): ?>
		<?= $this->Html->script("product.js"); ?>
	<?php endif; ?>

	<?php if (
		$url == $this->Url->build(['_name' => 'AddProduct']) || 
		$url == $this->Url->build(['_name' => 'Registration']) ||
		$url == $this->Url->build(['_name' => 'EditUser']) || 
		$url == $this->Url->build(['_name' => 'Registration'])
	): ?>
		<?= $this->Html->script("image.js"); ?>
	<?php endif; ?>

	<?php if (
		$url == $this->Url->build(['_name' => 'HomePage']) || 
		$url == $this->Url->build(['_name' => 'ViewAllUser'])
	): ?>
		<?= $this->Html->script("popper.min.js"); ?>
		<?= $this->Html->script("bootbox.all.min.js"); ?>
	<?php endif; ?>

</head>
<body>
	<?= $this->fetch('content'); ?>
</body>
</html>