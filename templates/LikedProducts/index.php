<?= $this->fetch('header'); ?>
<br>
<?= $this->Flash->render() ?>
<br>
<div class="container">
    <div class="jumbotron">
        <?php echo $this->cell('Product::productList',[$products,true])->render('product_list'); ?>
    </div>
</div>