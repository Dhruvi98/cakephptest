<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?= $this->Identity->get('email') ?>
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="<?= $this->Url->build(['_name' => 'Logout']) ?>">logout</a>
					<a class="dropdown-item" href="<?= $this->Url->build(['_name' => 'EditUser',$this->Identity->get('id')]) ?>">Edit Profile</a>
				</div>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Products
				</a>
				<?php if ($this->Identity->get('role') === 'Admin'): ?>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?= $this->Url->build(['_name' => 'HomePage']) ?>">All Products</a>
						<a class="dropdown-item" href="<?= $this->Url->build(['_name' => 'AddProduct']) ?>">Add Product</a>
					</div>
				<?php else: ?>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?= $this->Url->build(['_name' => 'ViewAllProduct']) ?>">All Products</a>
						<a class="dropdown-item" href="<?= $this->Url->build(['_name' => 'ViewAllCartProduct']) ?>">Cart</a>
						<a class="dropdown-item" href="<?= $this->Url->build(['_name' => 'ViewAllFavouriteProduct']) ?>">Favourite</a>
					</div>
				<?php endif; ?>
			</li>

			<?php if ($this->Identity->get('role') === 'Admin'): ?>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Users
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?= $this->Url->build(['_name' => 'ViewAllUser']) ?>">All Users</a>
					</div>
				</li>
			<?php endif; ?>
		</ul>
	</div>
</nav>
<br>
<div class="alert alert-success alert-dismissible" style="display: none;">
</div>
<div id="wait" style="display:none;position:absolute;top:50%;left:40%;padding:2px;background-color: white;">
	<?= $this->Html->image('loader.gif',['width' => '100%']); ?>
</div>