<?php if (!empty($products->toArray())): ?>
	<a href="<?= $this->Url->build(['_name' => 'HomePage']) ?>">back to home</a>
	<div class="table-responsive">
		<table class="table table-bordered" style="border:1px solid black">
			<tr align="center">
				<?php foreach ($products as $products => $product): ?>
					<td>
						<div class="card" style="width:300px;height: 600px">
							<div style="height: 200px">
								<?= $this->Html->image('uploads/'.$product->image,['class' => 'card-img-top','width' => '100%','height' => '100%']); ?>
							</div>
							<br>
							<?php echo $this->cell('Product::favouriteProduct',[$product,$favouriteProductPage])->render('favourite_product'); ?>

							<div class="card-body" style="height: 200px">
								<h4 class="card-title"><?= h($product->name) ?></h4>
								<p class="card-text"><?= h($product->description) ?></p>
								<p class="card-text">Price : <?= h($product->price) ?></p>

								<?php echo $this->cell('Product::cartProduct',[$product,$cartProductPage])->render('cart_product'); ?>

								<?php echo $this->cell('Product::socialMediaLink',[$product])->render('social_media_link'); ?>
								</div> 
							</td> 
						<?php endforeach; ?>
					</tr>
				</table>
			</div>
<?php else: ?>
	<?php if ($cartProductPage): ?>
		<div align="center">
			<?= $this->Html->image('empty_cart.png'); ?>
			<center><h5 style="font-family: times new roman"><a href="<?= $this->Url->build(['_name' => 'ViewAllProduct']) ?>">Shop Now</a></h5></center>
		</div>
	<?php endif; ?>
	<?php if ($favouriteProductPage): ?>
		<div align="center">
			<?= $this->Html->image('empty_wishlist.png'); ?>
			<center><h3 style="font-family: times new roman;color: red">Your WishList Is Empty</h3></center>
			<center><h5 style="font-family: times new roman;color: orange">Save Your Favourite Items Here</h5></center>
			<center><h5 style="font-family: times new roman"><a href="<?= $this->Url->build(['_name' => 'ViewAllProduct']) ?>">Add Favourite Items</a></h5></center>
		</div>
	<?php endif; ?>
<?php endif; ?>
	