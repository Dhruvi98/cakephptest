<a 
      href="https://www.facebook.com/sharer/sharer.php?u=http://localhost:8765/Products/view/<?= h($product->slug) ?>&t=<?= h($product->slug) ?>"
	onclick="javascript:window.open(this.href, '', menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
	target="_blank"
	title="Share on Facebook"
>
      <i class="fa fa-facebook-square" style="font-size:36px"></i>
</a>

<a 
      href="https://www.linkedin.com/shareArticle?mini=true&url=http://localhost:8765/Products/view/<?= h($product->slug) ?>&t=<?= h($product->slug) ?>"
      onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" 
      target="_blank"
      title="Share on Linkedin"
>
      <i class="fa fa-linkedin" style="font-size:36px"></i>
</a>

<a 
      href="http://pinterest.com/pin/create/button/?url=http://localhost:8765/Products/view/<?= h($product->slug) ?>&media=http://localhost:8765/img/uploads/<?= $product->image ?>"
      onClick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" 
      target="_blank"
      title="Share on Pinterest"
>
      <i class="fa fa-pinterest" style="font-size:36px"></i>
</a>