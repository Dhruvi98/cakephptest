<?php if(!empty($product->liked_products)): ?>
  <?php foreach($product->liked_products as $likedProduct): ?>
  	<?php if($this->Identity->get('id') == $likedProduct->user_id): ?>
  		<a data-modal-url="<?= $this->Url->build(['_name' => 'RemoveFromFavourite',$product->slug]) ?>" id="productOperation" style="cursor: pointer;">
  			<i class="fa fa-heart" style="font-size: 25px;color:red;"></i>
  		</a>
  	<?php else: ?>
  		<a  data-modal-url="<?= $this->Url->build(['_name' => 'AddToFavourite',$product->slug]) ?>" id="productOperation" style="cursor: pointer;">
        <i class="fa fa-heart-o" style="font-size: 25px;color:red;"></i>
      </a>
    <?php endif; ?>
  <?php endforeach; ?>
<?php else: ?>
  <?php if($favouriteProductPage): ?>
    <a  data-modal-url="<?= $this->Url->build(['_name' => 'RemoveFromFavourite',$product->slug]) ?>" id="productOperation" style="cursor: pointer;">
      <i class="fa fa-heart" style="font-size: 25px;color:red;"></i>
    </a>
  <?php else: ?>
    <a  data-modal-url="<?= $this->Url->build(['_name' => 'AddToFavourite',$product->slug]) ?>" id="productOperation" style="cursor: pointer;">
      <i class="fa fa-heart-o" style="font-size: 25px;color:red;"></i>
    </a>
  <?php endif; ?>
<?php endif; ?>