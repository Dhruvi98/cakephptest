<?php if ($product->quantity === 0): ?>
	<p align="center" style="color: red;font-style: bold;">Out Of Stock</p>
<?php else: ?>
	<?php if($cartProductPage): ?>
		<p align="center"><a data-modal-url="<?= $this->Url->build(['_name' => 'RemoveFromCart',$product->slug]) ?>" class="btn btn-primary" id="productOperation">Remove From Cart</a></p>
	<?php else: ?>
		<p align="center"><a data-modal-url="<?= $this->Url->build(['_name' => 'AddToCart',$product->slug]) ?>" class="btn btn-primary" id="productOperation">Add To Cart</a></p>
	<?php endif; ?>
<?php endif; ?>