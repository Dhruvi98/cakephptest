<?= $this->Flash->render() ?>
<br>
<div class="container">
	<div class="jumbotron">
		<div class="users form">
			<h3><center>Login</center></h3>
			<br>
			<?= $this->Form->create($loginForm) ?>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'email',
						[
							'type' => 'text',
							'required' => false,
							'placeholder' => "Enter Email ",
							'class' => ($this->Form->isFieldError('email')) ? 'form-control is-invalid' : 'form-control'
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'password',
						[
							'required' => false ,
							'class' => ($this->Form->isFieldError('password')) ? 'form-control is-invalid' : 'form-control',
							'type' => 'password',
							'placeholder' => 'Enter Password'
						]
					); ?>
					<p align="right"><a href="<?= $this->Url->build(['_name' => 'ForgetPassword']) ?>">Forget Password ?</a></p>
				</div>

			</div>
			<br>
			<div align="center">
				<?= $this->Form->submit(__('Login'),['class' => "btn btn-success"]); ?>
			</div>
			<?= $this->Form->end() ?>
			<br>
			<div align="center">
				<a href="<?= $this->Url->build(['_name' => 'Registration']) ?>">Don't have and account ?</a>
			</div>
		</div>
	</div>
</div>