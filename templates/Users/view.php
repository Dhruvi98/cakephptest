<?= $this->fetch('header'); ?>
<br>
<?= $this->Flash->render() ?>
<br>
<div class="container">
    <div class="jumbotron">
        <div id="loadData">
            <br>
            <div class="table-responsive">
                <table class="table table-bordered" style="border:1px solid black">
                    <thead>
                        <tr align="center">
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('name') ?></th>
                            <th><?= $this->Paginator->sort('email') ?></th>
                            <th><?= $this->Paginator->sort('status') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr align="center">
                                <td><?= $this->Number->format($user->id) ?></td>
                                <td><?= h($user->name) ?></td>
                                <td><?= h($user->email) ?></td>
                                <td>
                                    <?php if ($user->status === 'UnBlock'): ?>
                                        <input type="button" data-url= "<?= $this->Url->build(['_name' => 'BlockUser',$user->id]) ?>" id="ajaxBtn" class="btn btn-sm btn-danger" value="Block">
                                    <?php else: ?>
                                        <input type="button" data-url= "<?= $this->Url->build(['_name' => 'UnBlockUser',$user->id]) ?>" id="ajaxBtn" class="btn btn-sm btn-success" value="UnBlock">
                                    <?php endif; ?>
                                </td>
                                <td class="actions">
                                    <?= $this->Html->image("delete.png", [
                                        "alt" => "Delete",
                                        "id" => "ajaxBtn",
                                        'width' => "20",
                                        'data-url' => $this->Url->build(['_name' => 'DeleteUser',$user->id])
                                    ]); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="paginator">
                <ul class="pagination justify-content-end">
                    <?= $this->Paginator->prev("<<") ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(">>") ?>
                </ul>
                <p align="right"><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click',"#ajaxBtn",function() {
        var url = $(this).data("url");
        bootbox.confirm('Are you sure?', function(result) { 
            if (result) {
                $(document).find("#wait").css("display", "block");
                $.ajax({
                    url : url,
                    success: function(output) {
                        $('.alert-success').removeClass('alert-danger');
                        $('.alert-success').show();
                        $('.alert-success').text(output);
                        $('.container').empty();
                        $(".container").load(" .container",function(){
                            $(document).find("#wait").css("display", "none");
                        }); 
                    },
                    error: function(){
                        $('.alert-success').addClass('alert-danger');
                        $('.alert-success').show();
                        $('.alert-success').text("Internal Server Error. Please try again !");
                        $(document).find("#wait").css("display", "none");
                    }
                });
            }
        });
    });
</script>