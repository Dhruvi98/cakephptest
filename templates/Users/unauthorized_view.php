<?= $this->fetch('header'); ?>
<br>
<?= $this->Flash->render() ?>
<br>
<div class="container">
	<div class="jumbotron">
		<center><h3 style="font-family: times new roman">Oops ! You are not authorized person !</h3></center>
		<center><h3 style="font-family: times new roman"><a href="<?= $this->Url->build(['_name' => 'HomePage']) ?>">Back To Home Page</a></h3></center>
	</div>
</div>