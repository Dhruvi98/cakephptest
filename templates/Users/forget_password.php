<?= $this->Flash->render() ?>
<br>
<div class="container">
	<div class="jumbotron">
		<div class="users form">
			<h3><center>Forget Password</center></h3>
			<br>
			<?= $this->Form->create($emailForm) ?>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'email',
						[
							'type' => 'text',
							'label'=>'Enter your registered email address',
							'required' => false,
							'placeholder' => "Enter Email ",
							'class' => ($this->Form->isFieldError('email')) ? 'form-control is-invalid' : 'form-control'
						]
					);?>
				</div>
			</div>
			
			<div align="center">
				<?= $this->Form->submit(__('Send Link'),['class' => "btn btn-success",'name' => 'submit']); ?>
			</div>
			<?= $this->Form->end() ?>

		</div>
	</div>
</div>