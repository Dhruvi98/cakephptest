<br>
<div class="container">
	<div class="jumbotron">
		<div class="users form">
			<h3><center>Restet Password</center></h3>
			<br>
			<?= $this->Form->create($resetPasswordForm) ?>
			<?= $this->Form->hidden('token',['value'=> $this->getRequest()->getQuery('token')]); ?>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'password',
						[
							'required' => false,
							'type' => 'password',
							'class' => ($this->Form->isFieldError('password')) ? 'form-control is-invalid' : 'form-control',
							'placeholder' => "Enter New password"
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'confirm_password',
						[
							'required' => false,
							'type' => 'password',
							'class' => ($this->Form->isFieldError('confirm_password')) ? 'form-control is-invalid' : 'form-control',
							'placeholder' => "Enter Confirm password"
						]
					);?>
				</div>
			</div>
			<br>
			<br>
			<div align="center">
				<?= $this->Form->submit(__('Reset Password'),['class' => "btn btn-success",'name' => 'reset']); ?>
			</div>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>