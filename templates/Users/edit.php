<?= $this->fetch('header'); ?>
<br>
<?= $this->Flash->render() ?>
<br>
<div class="container">
	<div class="jumbotron">
		<div class="users form">
			<h3><center>Update My Profile</center></h3>
			<br>
			<?= $this->Form->create($user,['type' => 'file']) ?>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'name',
						[
							'required' => false,
							'type' => 'text',
							'class' => ($this->Form->isFieldError('name')) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm',
							'placeholder' => "Enter Name"
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'email',
						[
							'required' => false,
							'type' => 'text',
							'class' => ($this->Form->isFieldError('email')) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm',
							'placeholder' => "Enter Email"
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'profile_image',
						[
							'required' => false,
							'type' => 'file',
							'id' => 'image',
							'class' => ($this->Form->isFieldError('profile_image')) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm',
							'placeholder' => "Enter Confirm password"
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6" style="border: 1px solid black;">
					<?= $this->Html->image('uploads/'.$user->profile_image,['width' => '200' ,'id' => 'photo']) ?>
				</div>
			</div>
			<br>
			<div align="center">
				<?= $this->Form->submit(__('Update Profile'),['class' => "btn btn-success",'name' => 'submit']); ?>
			</div>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).on("change","#image",function() {

        var changedFile = $("#image").val();

        var data = changedFile.replace(changedFile.substring(0, 12),"");
        $("#imageError").text("");
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $("#photo").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
    });
</script>