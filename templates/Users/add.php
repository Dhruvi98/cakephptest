<br>
<div class="container">
	<div class="jumbotron">
		<div class="users form">
			<h3><center>Registration</center></h3>
			<br>
			<?= $this->Form->create($user,['type' => 'file']) ?>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'name',
						[
							'required' => false,
							'type' => 'text',
							'class' => ($this->Form->isFieldError('name')) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm',
							'placeholder' => "Enter Name"
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'email',
						[
							'required' => false,
							'type' => 'text',
							'class' => ($this->Form->isFieldError('email')) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm',
							'placeholder' => "Enter Email"
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'password',
						[
							'required' => false,
							'type' => 'password',
							'class' => ($this->Form->isFieldError('password')) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm',
							'placeholder' => "Enter password"
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'confirm_password',
						[
							'required' => false,
							'type' => 'password',
							'class' => 'form-control',
							'class' => ($this->Form->isFieldError('confirm_password')) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm',
							'placeholder' => "Enter Confirm password"
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<?= $this->Form->control(
						'profile_image',
						[
							'required' => false,
							'type' => 'file',
							'id' => 'image',
							'class' => ($this->Form->isFieldError('profile_image')) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm',
							'placeholder' => "Enter Confirm password"
						]
					);?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6" style="border: 1px solid black;">
            		<?= $this->Html->image('preview.png',['width' => '200' ,'id' => 'photo']) ?>
            	</div>
            </div>
            <br>
			<div align="center">
				<?= $this->Form->submit(__('Registration'),['class' => "btn btn-success",'name' => 'submit']); ?>
			</div>
			<?= $this->Form->end() ?>
			<br>
			<div align="center">
				<a href="<?= $this->Url->build(['_name' => 'Login']) ?>">Alredy have an account ?</a>
			</div>
		</div>
	</div>
</div>
