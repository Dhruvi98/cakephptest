<?php
declare(strict_types=1);

namespace App\View\Cell;

use Cake\View\Cell;

class ProductCell extends Cell
{
    /**
     * show product's list
     * @param object $products object of product
     * @param bool $favouriteProductPage set favourite page
     * @param bool $cartProductPage set cart page
     * @return void
     */
    public function productList($products, $favouriteProductPage = false, $cartProductPage = false): void
    {
        $this->set('favouriteProductPage', $favouriteProductPage);
        $this->set('cartProductPage', $cartProductPage);
        $this->set('products', $products);
    }

    /**
     * set product as favourite product in view
     * @param object $product object of product
     * @param bool $favouriteProductPage set favourite page
     * @return void
     */
    public function favouriteProduct($product, $favouriteProductPage = false): void
    {
        $this->set('favouriteProductPage', $favouriteProductPage);
        $this->set('product', $product);
    }

    /**
     * set add to cart button or remove from cart
     * @param object $product object of product
     * @param bool $cartProductPage set cart page
     * @return void
     */
    public function cartProduct($product, $cartProductPage = false): void
    {
        $this->set('cartProductPage', $cartProductPage);
        $this->set('product', $product);
    }

    /**
     * social links
     * @param object $product object of product
     * @return void
     */
    public function socialMediaLink($product): void
    {
        $this->set('product', $product);
    }
}
