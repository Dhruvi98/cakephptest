<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\User;
use Authorization\IdentityInterface;

class UserPolicy
{
    /**
     * Only admin can view user's list
     *
     * @param \App\Policy\Authorization\IdentityInterface $userIdentity logged user's data
     * @param \App\Model\Entity\User $user user's data
     * @return bool
     */
    public function canView(IdentityInterface $userIdentity, User $user)
    {
        return $this->isAdmin($userIdentity, $user);
    }

    /**
     * Logged user can edit only their profile
     *
     * @param \App\Policy\Authorization\IdentityInterface $userIdentity logged user's data
     * @param \App\Model\Entity\User $user user's data
     * @return bool
     */
    public function canEdit(IdentityInterface $userIdentity, User $user)
    {
        return $userIdentity->id === $user->id;
    }

    /**
     * Only admin can delete user's data
     *
     * @param \App\Policy\Authorization\IdentityInterface $userIdentity logged user's data
     * @param \App\Model\Entity\User $user user's data
     * @return bool
     */
    public function canDelete(IdentityInterface $userIdentity, User $user)
    {
        return $this->isAdmin($userIdentity, $user);
    }

    /**
     * Check weather user is admin or not
     *
     * @param \App\Policy\Authorization\IdentityInterface $userIdentity logged user's data
     * @return bool
     */
    protected function isAdmin(IdentityInterface $userIdentity)
    {
        if ($userIdentity->role === "Admin") {
            return true;
        } else {
            return false;
        }
    }
}
