<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Product;
use Authorization\IdentityInterface;

class ProductPolicy
{
    /**
     * Only admin can add product
     *
     * @param \App\Policy\Authorization\IdentityInterface $user logged user's data
     * @param \App\Model\Entity\Product $product data of product
     * @return bool
     */
    public function canAdd(IdentityInterface $user, Product $product)
    {
        return $this->isAdmin($user, $product);
    }

    /**
     * Only admin can edit product
     *
     * @param \App\Policy\Authorization\IdentityInterface $user logged user's data
     * @param \App\Model\Entity\Product $product data of product
     * @return bool
     */
    public function canEdit(IdentityInterface $user, Product $product)
    {
        return $this->isAdmin($user, $product);
    }

    /**
     * Only admin can delete product
     *
     * @param \App\Policy\Authorization\IdentityInterface $user logged user's data
     * @param \App\Model\Entity\Product $product data of products
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Product $product)
    {
        return $this->isAdmin($user, $product);
    }

    /**
     * Check weather user is admin or not
     *
     * @param \App\Policy\Authorization\IdentityInterface $user logged user's data
     * @return bool
     */
    protected function isAdmin(IdentityInterface $user)
    {
        if ($user->role == "Admin") {
            return true;
        } else {
            return false;
        }
    }
}
