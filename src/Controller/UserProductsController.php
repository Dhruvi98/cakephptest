<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;

class UserProductsController extends AppController
{
    /**
     * Display thumnail of products selected by logged user
     *
     * @return void
     */
    public function index(): void
    {
        $user = $this->request->getAttribute('identity');
        $products = $this->UserProducts
                    ->find()
                    ->contain(['Products','Products.LikedProducts'])
                    ->where(['UserProducts.user_id' => $user->id]);

        $products = $products->extract('product');
        $this->set(compact('products'));
    }

    /**
     * Add product into cart
     *
     * @param string $slug product-slug
     * @return \Cake\Http\Response|null
     */
    public function addToCart($slug = null)
    {
        $product = $this->validateSlug($slug);
        if ($product != null) {
            $now = FrozenTime::now();
            $currentDate = $now->year . "-" . $now->month . "-" . $now->day;
            $product = $this->UserProducts->Products->get($product->id);
            $product->set(['quantity' => --$product->quantity]);

            if ($this->UserProducts->Products->save($product)) {
                $userProduct = $this->UserProducts->newEmptyEntity();
                $user = $this->request->getAttribute('identity');
                $data = [
                    'user_id' => $user->id,
                    'product_id' => $product->id,
                    'date' => $currentDate,
                ];

                $userProduct = $this->UserProducts->patchEntity($userProduct, $data);
                if ($this->UserProducts->save($userProduct)) {
                    $this->setResponse(
                        $this->getResponse()->withStringBody(
                            'The product has been added to the cart.'
                        )
                    );
                } else {
                    $this->setResponse(
                        $this->getResponse()->withStringBody(
                            'The product could not be added to the cart.'
                        )
                    );
                }

                return $this->getResponse();
            }
        }
    }

    /**
     * Remove product from cart
     *
     * @param string $slug product-slug
     * @return \Cake\Http\Response|null
     */
    public function removeFromCart($slug = null)
    {
        $product = $this->validateSlug($slug);
        $product = $this->UserProducts->Products->get($product->id);
        $product->set(['quantity' => ++$product->quantity]);

        if ($this->UserProducts->Products->save($product)) {
            $product_id = $product->id;
            $user_id = $this->request->getAttribute('identity')->id;
            $product = $this->UserProducts->find()
                        ->where(['product_id' => $product_id , 'user_id' => $user_id])
                        ->first();

            if ($this->UserProducts->delete($product)) {
                $this->setResponse(
                    $this->getResponse()->withStringBody(
                        'The product has been removed from the cart.'
                    )
                );
            } else {
                $this->setResponse(
                    $this->getResponse()->withStringBody(
                        'The product could not be removed from the cart. please try again !'
                    )
                );
            }

            return $this->getResponse();
        }
    }
}
