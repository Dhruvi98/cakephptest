<?php
declare(strict_types=1);

namespace App\Controller;

class ProductsController extends AppController
{
    /**
     * Initialization hook method
     * Use this method to add common initialization code like loading components, initialize variables.
     *
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('FileUploader');
    }

    /**
     * Check user role after login , according to role the home page will display
     *
     * @return void
     */
    public function index(): void
    {
        $user = $this->request->getAttribute('identity');
        if ($user->role == 'Admin') {
            $products = $this->paginate($this->Products, ['limit' => 3]);
            $this->set(compact('products'));
            $this->render('admin_home');
        } else {
            $this->render('user_home');
        }
    }

    /**
     * Thumbnail of products
     *
     * @param string|null $slug product-slug
     * @return void
     */
    public function view($slug = null): void
    {
        $products = $this->Products->find()->contain(['LikedProducts']);
        // if productname is empty then all product will display
        if (empty($slug)) {
            $this->set(compact('products'));
        } else {
            $products = $products->where(['slug like' => $slug]);
            // if product is null than product not found page will display
            if ($products->first() === null) {
                $this->set('message', "Product Not Found !");
                $this->render('invalid_url');
            } else {
                $this->set('products', $products);
            }
        }
    }

    /**
     * Add product into database
     *
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        $this->isAuthorizedUser('add');
        $product = $this->Products->newEmptyEntity();
        if ($this->getRequest()->getData('submit')) {
            $product = $this->Products->patchEntity($product, $this->getRequest()->getData());
            $fileName = $this->FileUploader->generateFileName('image');
            if (empty($product->getErrors())) {
                $product->image = $fileName;
                if ($this->Products->save($product)) {
                    if ($this->FileUploader->uploadFile('image')) {
                        $this->Flash->success(__("Product has been saved."));

                        return $this->redirect(['_name' => 'HomePage']);
                    }
                }
                $this->Flash->error(__("Product could not be saved. please try again !"));
            }
        }
        $this->set(compact('product'));
    }

    /**
     * Edit product data
     *
     * @param string|null $slug product-slug
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function edit($slug = null)
    {
        $this->isAuthorizedUser('edit');
        $product = $this->validateSlug($slug);
        if ($this->getRequest()->getData('submit')) {
            $fileName = $this->FileUploader->generateFileName('image');
            if (empty($fileName)) {
                $fileName = $product->image;
            } else {
                $this->FileUploader->uploadFile('image');
            }
            $product = $this->Products->patchEntity($product, $this->getRequest()->getData());
            $product->image = $product->getOriginal('image');

            if (empty($product->getErrors())) {
                $product->image = $fileName;
                if ($this->Products->save($product)) {
                    $this->Flash->success(__("Product has been saved."));

                    return $this->redirect(['_name' => 'HomePage']);
                }
                $this->Flash->error(__("Product could not be saved. please try again !"));
            }
        }
        $this->set(compact('product'));
    }

    /**
     * Delete product data
     *
     * @param string|null $slug product-slug
     * @return \Cake\Http\Response|null
     */
    public function delete($slug = null)
    {
        $this->isAuthorizedUser('delete');
        $product = $this->validateSlug($slug);
        if ($this->Products->delete($product)) {
            $this->setResponse(
                $this->getResponse()->withStringBody(
                    'Product has been deleted'
                )
            );
        } else {
            $this->setResponse(
                $this->getResponse()->withStringBody(
                    'Product could not be deleted. please try again !'
                )
            );
        }

        return $this->getResponse();
    }

    /**
     * Check weather user is authorized or not
     *
     * @param string $operation controller's action
     * @return bool
     */
    public function isAuthorizedUser($operation)
    {
        $userIdentity = $this->request->getAttribute('identity');
        $product = $this->Products->newEmptyEntity();
        if ($userIdentity->can($operation, $product)) {
            return true;
        }
        $this->render('unauthorized_view');
    }
}
