<?php
declare(strict_types=1);

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime;

class FileUploaderComponent extends Component
{
    // upload directory path
    private $targetDir = '';
    private $fileName = "";
    private $controller = '';
    private $request = '';

    /**
     * Initialization hook method
     * Use this method to add common initialization code like loading components, initialize variables.
     *
     * @param array $config configayration
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->controller = $this->getController();
        $this->targetDir = Configure::read('App.uploadBaseUrl');
        $this->request = $this->controller->getRequest();
    }

    /**
     * Generate filename like 1586755289-2992.jpeg
     *
     * @param string $fieldName  file input name attribute
     * @return string $fileName
     */
    public function generateFileName($fieldName)
    {
        $attachement = $this->request->getData($fieldName);
        $fileName = $attachement['name'];
        if (!empty($fileName)) {
            $fourdigitrandom = rand(1000, 9999);
            $timestamp = FrozenTime::now()->toUnixString();
            $extension = pathinfo($fileName, PATHINFO_EXTENSION);
            $fileName = basename($timestamp . "-" . $fourdigitrandom . "." . $extension);
            $this->fileName = $fileName;
        }

        return $fileName;
    }

    /**
     * Upload files into specified directory
     *
     * @param string $fieldName  file input name attribute
     * @return bool
     */
    public function uploadFile($fieldName)
    {
        $attachement = $this->request->getUploadedFile($fieldName);
        $attachement->moveTo($this->targetDir . $this->fileName);

        if ($this->request->getUploadedFiles()) {
            return true;
        }
    }
}
