<?php
declare(strict_types=1);

namespace App\Controller;

class LikedProductsController extends AppController
{
    /**
     * Shows favourite product list
     *
     * @return void
     */
    public function index()
    {
        $user = $this->request->getAttribute('identity');
        $products = $this->LikedProducts
                    ->find()
                    ->contain(['Products'])
                    ->where(['LikedProducts.user_id' => $user->id]);

        $products = $products->extract('product');
        $this->set(compact('products'));
    }

    /**
     * Add products to cart products
     *
     * @param string $slug  product-slug
     * @return \Cake\Http\Response
     */
    public function addToFavourite($slug)
    {
        $product = $this->validateSlug($slug);
        if ($product != null) {
            $product = $this->LikedProducts->Products->get($product->id);
            $likedProduct = $this->LikedProducts->newEmptyEntity();
            $user = $this->request->getAttribute('identity');

            $data = [
                'user_id' => $user->id,
                'product_id' => $product->id,
            ];

            $likedProduct = $this->LikedProducts->patchEntity($likedProduct, $data);

            if ($this->LikedProducts->save($likedProduct)) {
                $this->setResponse(
                    $this->getResponse()->withStringBody(
                        'The product has been added to the favourite list.'
                    )
                );
            } else {
                $this->setResponse(
                    $this->getResponse()->withStringBody(
                        'The product could not be added to the favourite list.'
                    )
                );
            }

            return $this->getResponse();
        }
    }

    /**
     * Remove products from cart products
     *
     * @param string $slug  product-slug
     * @return \Cake\Http\Response
     */
    public function removeFromFavourite($slug)
    {
        $product = $this->validateSlug($slug);
        if ($product != null) {
            $product_id = $product->id;
            $user_id = $this->request->getAttribute('identity')->id;
            $product = $this->LikedProducts->find()
                    ->where(['product_id' => $product_id , 'user_id' => $user_id])
                    ->first();

            if ($this->LikedProducts->delete($product)) {
                $this->setResponse(
                    $this->getResponse()->withStringBody(
                        'The product has been removed from the favourite list.'
                    )
                );
            } else {
                $this->setResponse(
                    $this->getResponse()->withStringBody(
                        'The product could not be removed from the favourite list.'
                    )
                );
            }

            return $this->getResponse();
        }
    }
}
