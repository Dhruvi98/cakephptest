<?php
declare(strict_types=1);

namespace App\Controller;

use App\Event\UserListener;
use App\Form\EmailForm;
use App\Form\LoginForm;
use App\Form\ResetPasswordForm;
use Cake\Event\Event;
use Cake\Utility\Security;
use Cake\Utility\Text;
use Cake\I18n\FrozenTime;

class UsersController extends AppControllers
{
    /**
     * Initialization hook method
     * Use this method to add common initialization code like loading components, initialize variables.
     * 
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();
        $listener = new UserListener();
        $this->getEventManager()->on($listener);
        $this->loadComponent('FileUploader');
    }

    /**
     * User can access only fogetpassword , add , resetpassword pages without login
     *
     * @param \Cake\Event\EventInterface $event An Event instance
     * @return \Cake\Http\Response|null|void
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['login','forgetPassword','add','resetPassword']);
    }

    /**
     * Display lists of users
     *
     * @return void
     */
    public function view(): void
    {
        $user = $this->Users->newEmptyEntity();
        $this->isAuthorizedUser('view', $user);
        $users = $this->paginate($this->Users, ['limit' => 3]);
        $this->set(compact('users'));
    }

    /**
     * Add user's data into database
     *
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $fileName = $this->FileUploader->generateFileName('profile_image');
            if (empty($user->getErrors())) {
                if (!empty($fileName)) {
                    $user->profile_image = $fileName;
                }
                if ($this->Users->save($user)) {
                    if ($this->FileUploader->uploadFile('profile_image')) {
                        $this->Flash->success(__("User has been saved."));

                        return $this->redirect(['_name' => 'Login']);
                    }
                }
                $this->Flash->error(__("User could not be saved. please try again !"));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Edit user's data
     *
     * @param int $id user's id
     * @return \Cake\Http\Response|null
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id);
        $this->isAuthorizedUser('edit', $user);
        if ($this->getRequest()->getData('submit')) {
            $fileName = $this->FileUploader->generateFileName('profile_image');

            if (empty($fileName)) {
                $fileName = $user->profile_image;
            } else {
                $this->FileUploader->uploadFile('profile_image');
            }

            $user = $this->Users->patchEntity($user, $this->getRequest()->getData());
            $user->profile_image = $user->getOriginal('profile_image');
            if (empty($user->getErrors())) {
                $user->profile_image = $fileName;
                if ($this->Users->save($user)) {
                    $this->Flash->success(__("Your profile has been updated."));

                    return $this->redirect(['_name' => 'HomePage']);
                }
                $this->Flash->error(__("Your profile could not be updated. please try again !"));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Checks login credentials of users
     *
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        $loginForm = new LoginForm();
        if ($loginForm->execute($this->request->getData())) {
            $result = $this->Authentication->getResult();
            if ($this->getRequest()->is('post')) {
                if ($result->isValid()) {
                    $status = $this->getRequest()->getAttribute('identity')->status;
                    if (!empty($status) && $status === 'UnBlock') {
                        $redirect = $this->request->getQuery('redirect', [
                            'controller' => 'Products',
                            'action' => 'index',
                        ]);
                        $this->Flash->success(__('You are successfully logged in !'));

                        return $this->redirect($redirect);
                    } else {
                        $this->Authentication->logout();
                        $this->Flash->error(__('Account is blocked. please contact to admin !'));
                    }
                }
                if ($this->request->is('post') && !$result->isValid()) {
                    $this->Flash->error(__('Invalid username or password'));
                }
            }
        }
        $this->set('loginForm', $loginForm);
    }

    /**
     * Destroy user session
     *
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        $this->Authentication->logout();
        $this->Flash->success(__('You are successfully logged out !'));

        return $this->redirect(['_name' => 'Login']);
    }

    /**
     * Send reset password link
     *
     * @return \Cake\Http\Response|null
     */
    public function forgetPassword()
    {
        $emailForm = new EmailForm();
        if ($emailForm->execute($this->getRequest()->getData())) {
            if ($this->getRequest()->is('post')) {
                $expireTime = FrozenTime::now()->modify('+20 minutes')->toUnixString();
                $email = $this->getRequest()->getData('email');
                $user = $this->Users->find()->where(['email' => $email])->first();

                if (!($user === null)) {
                    $user->token = $this->generateToken();
                    $user->expire_token = $expireTime;
                    if ($this->Users->save($user)) {
                        $event = new Event('SendResetPasswordLink', $this, ['user' => $user]);
                        $this->getEventManager()->dispatch($event);
                        $this->Flash->success("Reset Password Link Sent , Please check your email !");

                        return $this->redirect(['_name' => 'Login']);
                    }
                }
                $this->Flash->error(__('Email id is not exist !'));
            }
        }
        $this->set('emailForm', $emailForm);
    }

    /**
     * Reset password if valid token
     *
     * @return \Cake\Http\Response|null
     */
    public function resetPassword()
    {
        $resetPasswordForm = new ResetPasswordForm();

        if ($this->getRequest()->getData('reset')) {
            if ($resetPasswordForm->execute($this->getRequest()->getData())) {
                $currentTime = $expireTime = FrozenTime::now()->toUnixString();
                $token = $this->getRequest()->getData('token');
                $user = $this->Users->find()->where(['token' => $token])->first();

                if (!($user === null)) {
                    if (!($currentTime >= $user->expire_token)) {
                        $user->set(['password' => $this->getRequest()->getData('password')]);
                        $user->token = $this->generateToken();

                        if ($this->Users->save($user)) {
                            $this->Flash->success("Password Changed !");
                            $this->Authentication->logout();

                            return $this->redirect(['_name' => 'Login']);
                        }
                    }
                }
                $this->Flash->error(__('Token is expired ! Create new reset password request !'));

                return $this->redirect(['_name' => 'Login']);
            }
        }
        $this->set('resetPasswordForm', $resetPasswordForm);
    }

    /**
     * Generate unique token
     *
     * @return string
     */
    public function generateToken()
    {
        $password = sha1(Text::uuid());
        $password_token = Security::hash($password, 'sha256', true);

        return $password_token;
    }

    /**
     * Set user's status as block
     *
     * @param int $id user's id
     * @return \Cake\Http\Response|null
     */
    public function blockUser($id = null)
    {
        $user = $this->Users->get($id);
        $user->status = 'Block';

        if ($this->Users->save($user)) {
            $this->setResponse($this->getResponse()->withStringBody(
                    'The user has been blocked.'
                )
            );
        } else {
            $this->setResponse(
                $this->getResponse()->withStringBody(
                    'The user could not be blocked. please try again !'
                )
            );
        }

        return $this->getResponse();
    }

    /**
     * Set user'status as unblock
     *
     * @param int $id user's id
     * @return \Cake\Http\Response|null
     */
    public function unBlockUser($id = null)
    {
        $user = $this->Users->get($id);
        $user->status = 'UnBlock';

        if ($this->Users->save($user)) {
            $this->setResponse(
                $this->getResponse()->withStringBody(
                    'The user has been unblocked.'
                )
            );
        } else {
            $this->setResponse(
                $this->getResponse()->withStringBody(
                    'The user could not be unblocked. please try again !'
                )
            );
        }

        return $this->getResponse();
    }

    /**
     * Delete user data
     *
     * @param int $id user's id
     * @return \Cake\Http\Response|null
     */
    public function delete($id = null)
    {
        $user = $this->Users->get($id);

        if ($this->Users->delete($user)) {
            $this->setResponse(
                $this->getResponse()->withStringBody(
                    'The user has been deleted.'
                )
            );
        } else {
            $this->setResponse(
                $this->getResponse()->withStringBody(
                    'The user could not be deleted. please try again !'
                )
            );
        }

        return $this->getResponse();
    }

    /**
     * Check weather user is authorized or not
     *
     * @param string|null $operation controller's action
     * @param \App\Model\Entity\User $entity user data
     * @return bool
     */
    public function isAuthorizedUser($operation, $entity)
    {
        $userIdentity = $this->request->getAttribute('identity');
        if ($userIdentity->can($operation, $entity)) {
            return true;
        }
        $this->render('unauthorized_view');
    }
}
