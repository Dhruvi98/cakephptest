<?php
declare(strict_types=1);

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class LoginForm extends Form
{
    /**
     * Add field into schema
     *
     * @param \Cake\Form\Schema $schema The schema to customize.
     * @return \Cake\Form\Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        return $schema
            ->addField('email', ['type' => 'string'])
            ->addField('password', ['type' => 'text']);
    }

    /**
     * Returns the default validator object
     * to add a default validation set to the validator object.
     *
     * @param \Cake\Validation\Validator $validator validatorObject
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->notEmptyString('email', "Please Enter Email Id !")
            ->notEmptyString('password', "Please Enter Password !")
            ->add('email', 'email', ['message' => "Please Enter Valid Email Id !"]);

        return $validator;
    }

    /**
     * Set the errors in the form.
     *
     * @param array $errors Errors list.
     * @return void
     */
    public function setErrors($errors): void
    {
        $this->_errors = $errors;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data): bool
    {
        return true;
    }
}
