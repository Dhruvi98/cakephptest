<?php
declare(strict_types=1);

namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class ResetPasswordForm extends Form
{
    /**
     * Add fields into schema
     *
     * @param \Cake\Form\Schema $schema The schema to customize.
     * @return \Cake\Form\Schema The schema to use.
     */
    protected function _buildSchema(Schema $schema): Schema
    {
        return $schema
            ->addField('password', ['type' => 'text']);
    }

    /**
     * Returns the default validator object.
     * to add a default validation set to the validator object.
     *
     * @param \Cake\Validation\Validator $validator ValidatorObject
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('password')
            ->notEmptyString('password', 'Please Enter Password !')
            ->regex(
                'password',
                '/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/',
                "Password must contains uppercase, lowercase , digits and special characters !",
                ['last' => true]
            )
            ->add('password', 'length', [
                'rule' => ['minLength',8] ,
                'last' => true ,
                'message' => 'Password Length must be greater than 8 !',
            ]);

        $validator
            ->requirePresence('confirm_password')
            ->notEmptyString('confirm_password', 'Please Enter Confirm Password !')
            ->add('confirm_password', 'no-misspelling', [
                'rule' => ['compareWith', 'password'],
                'message' => 'Passwords are not equal',
            ]);

        return $validator;
    }

    /**
     * Set the errors in the form.
     *
     * @param array $errors Errors list.
     * @return void
     */
    public function setErrors($errors): void
    {
        $this->_errors = $errors;
    }

    /**
     * Hook method to be implemented in subclasses.
     *
     * Used by `execute()` to execute the form's action.
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data): bool
    {
        return true;
    }
}
