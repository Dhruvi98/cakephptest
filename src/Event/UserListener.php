<?php
declare(strict_types=1);

namespace App\Event;

use Cake\Event\EventListenerInterface;
use Cake\Mailer\MailerAwareTrait;

class UserListener implements EventListenerInterface
{
    use MailerAwareTrait;

    /**
     * Returns a list of events this object is implementing. When the class is registered
     * in an event manager, each individual method will be associated with the respective event.
     *
     * @return array Associative array or event key names pointing to the function
     * that should be called in the object when the respective event is fired
     */
    public function implementedEvents(): array
    {
        return [
            'SendResetPasswordLink' => 'sendResetPasswordMail',
        ];
    }

    /**
     * Send reset password link through mail
     *
     * @param \Cake\Event\Event $event eventObject
     * @param \App\Model\Entity\User $user user data
     * @return void
     */
    public function sendResetPasswordMail($event, $user): void
    {
        $this->getMailer('User')->send('resetPassword', [$user]);
    }
}
