<?php
declare(strict_types=1);

namespace App\Model\Behavior;

use Cake\ORM\Behavior;

class SluggableBehavior extends Behavior
{
    /**
     * Generate unique slug
     *
     * @param string $value users'name
     * @return string $slug
     */
    public function slug($value)
    {
        $data = [];
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $value)));
        $product = $this->getTable()->find('all')->select(['slug'])->where(['slug like ' => $slug . '%'])->toArray();
        foreach ($product as $product) {
            $data[] = $product->slug;
        }
        if (count($data) > 0) {
            if (in_array($slug, $data)) {
                $count = 0;
                while (in_array(($slug . '-' . ++$count ), $data));
                $slug = $slug . '-' . $count;
            }
        }

        return $slug;
    }
}
