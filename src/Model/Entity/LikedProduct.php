<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

class LikedProduct extends Entity
{
    protected $_accessible = [
        'user_id' => true,
        'product_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'product' => true,
    ];
}
