<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\ORM\Entity;

class User extends Entity
{
    protected $_accessible = [
        'name' => true,
        'email' => true,
        'password' => true,
        'role' => true,
        'token' => true,
        'expire_token' => true,
        'status' => true,
        'profile_image' => true,
        'created' => true,
        'modified' => true,
        'user_product' => true,
    ];

    protected $_hidden = [
        'password',
    ];

    /**
     *
     * Set hashed password
     *
     * @param string $password users's password
     * @return string hashedpassword
     */
    protected function _setPassword($password)
    {
        $hasher = new DefaultPasswordHasher();

        return $hasher->hash($password);
    }
}
