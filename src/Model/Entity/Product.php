<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Product extends Entity
{
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'quantity' => true,
        'price' => true,
        'image' => true,
        'slug' => true,
        'created' => true,
        'updated' => true,
        'user_product' => true,
        'liked_products' => true,
    ];
}
