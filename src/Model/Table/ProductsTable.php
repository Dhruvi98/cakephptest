<?php
declare(strict_types=1);

namespace App\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Sluggable');

        $this->hasMany('UserProduct', [
            'foreignKey' => 'product_id',
        ]);

        $this->hasMany('LikedProducts', [
            'foreignKey' => 'product_id',
        ]);
    }

    /**
     * Returns the default validator object
     * to add a default validation set to the validator object.
     *
     * @param \Cake\Validation\Validator $validator ValidatorObject
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('name')
            ->notEmptyString('name', "Please Enter Product Name !");

        $validator
            ->requirePresence('quantity', 'create')
            ->numeric('quantity', "Quantity Field allowed only digits !")
            ->notEmptyString('quantity', "Please Enter Quantity !");

        $validator
            ->requirePresence('price', 'create')
            ->numeric('price', "Price Field allowed only digits !")
            ->notEmptyString('price', "Please Enter Price !");

        $validator
            ->requirePresence('image', 'create')
            ->notEmptyFile('image', "Please Upload File !", 'create')
            ->add('image', [
                'mimeType' => [
                    'rule' => ['mimeType', ['image/jpg', 'image/jpeg' , 'image/png']],
                    'last' => true,
                    'message' => 'Please only upload images (jpeg, jpg , png ).',
                ],
                'fileSize' => [
                    'rule' => ['fileSize', '<=', '800000'],
                    'last' => true,
                    'message' => 'Cover image must be less than 800kb.',
                ],
            ]);

        return $validator;
    }

    /**
     * beforeSave callback.
     *
     * set slug if name field is dirty
     *
     * @param \Cake\Event\EventInterface $event The beforeSave event that was fired
     * @param \Cake\Datasource\EntityInterface $entity The entity that is going to be saved
     * @param \ArrayObject $options The options for the query
     * @return void
     */
    public function beforeSave(EventInterface $event, EntityInterface $entity, ArrayObject $options)
    {
        if ($entity->isDirty('name')) {
            $entity->slug = $this->slug($entity->name);
        }
    }
}
