<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('UserProduct', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->requirePresence('name')
            ->notEmptyString('name', 'Please Enter Name !')
            ->add('name', 'length', [
                'rule' => ['lengthBetween', 8, 100] ,
                'message' => 'Name Length must be greater than 8 !',
            ]);

        $validator
            ->requirePresence('email')
            ->notEmptyString('email', 'Please Enter Email Id !')
            ->add('email', 'email', ['message' => "Please Enter Valid Email Id !"]);

        $validator
            ->requirePresence('password', 'create')
            ->notEmptyString('password', 'Please Enter Password !', 'create')
            ->regex(
                'password',
                '/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/',
                "Password must contains uppercase, lowercase , digits and special characters !"
            )
            ->add('password', 'length', [
                'rule' => ['minLength',8] ,
                'last' => true ,
                'message' => 'Password Length must be greater than 8 !',
            ]);

        $validator
            ->requirePresence('confirm_password', 'create')
            ->notEmptyString('confirm_password', 'Please Enter Confirm Password !', 'create')
            ->add('confirm_password', 'no-misspelling', [
                'rule' => ['compareWith', 'password'],
                'message' => 'Passwords are not equal',
            ]);

        $validator
            ->requirePresence('profile_image', 'create')
            ->notEmptyFile('profile_image', "Please Upload File !", 'create')
            ->add('profile_image', [
                'mimeType' => [
                    'rule' => ['mimeType', ['image/jpg', 'image/jpeg' , 'image/png']],
                    'last' => true,
                    'message' => 'Please only upload images (jpeg, jpg , png ).',
                ],
                'fileSize' => [
                    'rule' => ['fileSize', '<=', '800000'],
                    'last' => true,
                    'message' => 'Cover image must be less than 800kb.',
                ],
            ]);

        return $validator;
    }

    /**
     * check user's email is is unique or not
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
