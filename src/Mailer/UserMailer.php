<?php
declare(strict_types=1);

namespace App\Mailer;

use Cake\Event\EventListenerInterface;
use Cake\Mailer\Mailer;

/**
 * User mailer.
 */
class UserMailer extends Mailer implements EventListenerInterface
{
    public static $name = 'User';

    /**
     * Send reset password mail
     *
     * @param \App\Model\Enitiy\User $user user's data
     * @return void
     */
    public function resetPassword($user): void
    {
        $this->setTransport('gmail')
             ->setEmailFormat('html')
             ->setTo($user->email)
             ->setSubject("Password Reset Link ")
             ->setViewVars(['user' => $user])
             ->viewBuilder()
             ->setTemplate('reset_mail');
    }
}
