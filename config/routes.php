<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

$routes->setRouteClass(DashedRoute::class);

$routes->scope('/', function (RouteBuilder $builder) {
    // Register scoped middleware for in scopes.
    $builder->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true,
    ]));
    $builder->applyMiddleware('csrf');

    $builder->connect('/login', ['controller' => 'Users', 'action' => 'login'],['_name' => 'Login']);
    $builder->connect('/logout', ['controller' => 'Users', 'action' => 'logout'],['_name' => 'Logout']);
    $builder->connect('/registration', ['controller' => 'Users', 'action' => 'add'],['_name' => 'Registration']);
    $builder->connect('/forget-password', ['controller' => 'Users', 'action' => 'forgetPassword'],['_name' => 'ForgetPassword']);
    $builder->connect('/reset-password', ['controller' => 'Users', 'action' => 'resetPassword'],['_name' => 'ResetPassword']);
    $builder->connect('/users/edit/**', ['controller' => 'Users', 'action' => 'edit'],['_name' => 'EditUser']);
    $builder->connect('/users/', ['controller' => 'Users', 'action' => 'view'],['_name' => 'ViewAllUser']);
    $builder->connect('/users/delete/**', ['controller' => 'Users', 'action' => 'delete'],['_name' => 'DeleteUser']);
    $builder->connect('/users/block/**', ['controller' => 'Users', 'action' => 'blockUser'],['_name' => 'BlockUser']);
    $builder->connect('/users/unblock/**', ['controller' => 'Users', 'action' => 'unBlockUser'],['_name' => 'UnBlockUser']);
    $builder->connect('/home', ['controller' => 'Products', 'action' => 'index'],['_name' => 'HomePage']);
    $builder->connect('/products/add', ['controller' => 'Products', 'action' => 'add'],['_name' => 'AddProduct']);
    $builder->connect('/products/edit/**', ['controller' => 'Products', 'action' => 'edit'],['_name' => 'EditProduct']);
    $builder->connect('/products/delete/**', ['controller' => 'Products', 'action' => 'delete'],['_name' => 'DeleteProduct']);
    $builder->connect('/products/**', ['controller' => 'Products', 'action' => 'view'],['_name' => 'ViewAllProduct']);
    $builder->connect('/addtocart/**', ['controller' => 'UserProducts', 'action' => 'addToCart'],['_name' => 'AddToCart']);
    $builder->connect('/removefromcart/**', ['controller' => 'UserProducts', 'action' => 'removeFromCart'],['_name' => 'RemoveFromCart']);
    $builder->connect('/products/cart', ['controller' => 'UserProducts', 'action' => 'index'],['_name' => 'ViewAllCartProduct']);
    $builder->connect('/addToFavourite/**', ['controller' => 'LikedProducts', 'action' => 'addToFavourite'],['_name' => 'AddToFavourite']);
    $builder->connect('/removeFromFavourite/**', ['controller' => 'LikedProducts', 'action' => 'removeFromFavourite'],['_name' => 'RemoveFromFavourite']);
    $builder->connect('/products/favourite', ['controller' => 'LikedProducts', 'action' => 'index'],['_name' => 'ViewAllFavouriteProduct']);

    $builder->fallbacks();
});

/*
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * $routes->scope('/api', function (RouteBuilder $builder) {
 *     // No $builder->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */
