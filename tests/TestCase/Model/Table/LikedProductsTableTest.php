<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LikedProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LikedProductsTable Test Case
 */
class LikedProductsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LikedProductsTable
     */
    protected $LikedProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.LikedProducts',
        'app.Users',
        'app.Products',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LikedProducts') ? [] : ['className' => LikedProductsTable::class];
        $this->LikedProducts = TableRegistry::getTableLocator()->get('LikedProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->LikedProducts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
