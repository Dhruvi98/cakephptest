<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserProductsTable Test Case
 */
class UserProductsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserProductsTable
     */
    protected $UserProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.UserProducts',
        'app.Users',
        'app.Products',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserProducts') ? [] : ['className' => UserProductsTable::class];
        $this->UserProducts = TableRegistry::getTableLocator()->get('UserProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->UserProducts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
