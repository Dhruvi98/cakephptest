$(".fa-heart-o").click(function() {
	$(this).toggleClass("fa-heart fa-heart-o");
});
$(".fa-heart").click(function() {
	$(this).toggleClass("fa-heart-o fa-heart");
});
$(document).ready(function(){

	$(document).on('click',"#productOperation",function(){
		var url = $(this).data("modal-url");
		$(document).find("#wait").css("display", "block");
		$.ajax({
			url : url,
			success: function(output) {
				$('.alert-success').removeClass('alert-danger');
				$('.alert-success').show();
				$('.alert-success').text(output);
				$('.container').empty();
				$(".container").load(" .container",function(){
					$(document).find("#wait").css("display", "none");
				});	
			},
			error: function(){
				$('.alert-success').addClass('alert-danger');
				$('.alert-success').show();
				$('.alert-success').text("Internal Server Error. Please try again !");
				$(document).find("#wait").css("display", "none");
			}
		});
	});
});